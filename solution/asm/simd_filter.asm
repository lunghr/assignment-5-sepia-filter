%define STACK_ALIGN 8

%define PIXEL_SIZE 3

global simd_filter

section .data
red_coefficients: dd 0.189, 0.769, 0.393, 0.0
green_coefficients: dd 0.168, 0.686, 0.349, 0.0
blue_coefficients: dd 0.131, 0.534, 0.272, 0.0

section .text
bits 64

simd_filter:
        sub rsp, STACK_ALIGN
        push r12
        push r13

        movdqu xmm0, [rel red_coefficients]
        movdqu xmm1, [rel green_coefficients]
        movdqu xmm2, [rel blue_coefficients]

        mov r12, rdi

        mov r8, [r12 + 8]
        xor ecx, ecx
    .loop_width:
        xor edx, edx
    .loop_height:
        movdqu xmm3, [r8]
        pmovzxbd xmm3, xmm3
        cvtdq2ps xmm3, xmm3

        movdqu xmm4, xmm3
        movdqu xmm5, xmm3

        mulps xmm3, xmm0
        mulps xmm4, xmm1
        mulps xmm5, xmm2

        haddps xmm3, xmm3
        haddps xmm3, xmm3
        haddps xmm4, xmm4
        haddps xmm4, xmm4
        haddps xmm5, xmm5
        haddps xmm5, xmm5

        cvtps2dq xmm3, xmm3
        cvtps2dq xmm4, xmm4
        cvtps2dq xmm5, xmm5

        mov r9, 0xFF
        extractps rax, xmm3, 0
        cmp rax, 0xFF
        cmovg rax, r9
        mov byte[r8 + 2], al
        extractps rax, xmm4, 0
        cmp rax, 0xFF
        cmovg rax, r9
        mov byte[r8 + 1], al
        extractps rax, xmm5, 0
        cmp rax, 0xFF
        cmovg rax, r9
        mov byte[r8], al

        add r8, PIXEL_SIZE
        inc edx
        cmp edx, dword[r12 + 4]
        jne .loop_height

        inc ecx
        cmp ecx, dword[r12 + 0]
        jne .loop_width

        mov rax, r12

        pop r13
        pop r12

        add rsp, STACK_ALIGN
        ret
