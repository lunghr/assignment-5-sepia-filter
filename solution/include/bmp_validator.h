#include "bmp_header.h"

#ifndef BMP_VALIDATOR_H
#define BMP_VALIDATOR_H

enum signature_file_status { UNKNOWN_FILE_SIGNATURE, BMP_FILE_SIGNATURE };

enum bmp_validation_status {
  INVALID_BMP_HEADER,
  INVALID_BMP_DATA,
  VALIDATION_SUCCESS,
  INVALID_FILE_FORMAT
};

enum signature_file_status check_file_signature(struct bmp_header *header);

enum bmp_validation_status validate_bmp(struct bmp_header *header,
                                        enum signature_file_status signature);

#endif
