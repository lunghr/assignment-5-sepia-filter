#include <stdint.h>

#ifndef BMP_HEADER_H
#define BMP_HEADER_H

#define BI_COMPRESSION 0
#define BI_SIZE_IMAGE 0
#define BI_PLANES 1
#define BI_INFORMATION_HEADER_SIZE 40
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0
#define BMP_SIGNATURE 0x4D42

#pragma pack(push, 1)
struct bmp_header {
  uint16_t bf_type;
  uint32_t bf_size;
  uint32_t bf_res;
  uint32_t bf_off_bits;
  uint32_t bi_size;
  uint32_t bi_width;
  uint32_t bi_height;
  uint16_t bi_planes;
  uint16_t bi_bit_count;
  uint32_t bi_compression;
  uint32_t bi_size_image;
  uint32_t bi_X_pels_mer_meter;
  uint32_t bi_Y_pels_per_meter;
  uint32_t bi_clr_used;
  uint32_t bi_clr_important;
};
#pragma pack(pop)

#endif
