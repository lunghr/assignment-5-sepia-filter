#include <stdio.h>

#ifndef FILE_OPEN_CLOSE_H
#define FILE_OPEN_CLOSE_H

enum open_status { OPEN_SUCCESS, OPEN_ERROR };

enum file_close_status { CLOSE_SUCCESS, CLOSE_ERROR };

enum open_status file_open_status(const char *fileName, FILE **file,
                                  const char *mode);

enum file_close_status close_status(FILE *file);

#endif
