#ifndef ROTATION_H
#define ROTATION_H

struct image *filter(struct image *image);

struct image *simd_filter(struct image *source_image);

#endif
