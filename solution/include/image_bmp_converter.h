#include "bmp_validator.h"
#include "image.h"
#include <stdbool.h>
#include <stdio.h>

#ifndef IMAGE_BMP_CONVERTER_H
#define IMAGE_BMP_CONVERTER_H

enum file_write_status { WRITE_SUCCESS, WRITE_ERROR };

bool read_header(FILE *file, struct bmp_header *header);

struct image *convert_to_image(struct bmp_header *header, FILE *file);

enum file_write_status convert_to_bmp(FILE *out, struct image *img);

#endif
