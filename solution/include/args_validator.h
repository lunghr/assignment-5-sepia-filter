#include <stdbool.h>

#ifndef ARGS_VALIDATOR_H
#define ARGS_VALIDATOR_H

enum angle_status {
  ANGLE_CORRECT,
  NOT_NUMERIC_ANGLE_VALUE,
  INVALID_ANGLE_VALUE
};

bool args_count_check(const int argc);

bool is_all_digits(const char *str);

enum angle_status angel_check(const char *potentional_angle_value);

#endif
