#include <stdint.h>
#include <stdlib.h>

#pragma once
#ifndef IMAGE_H
#define IMAGE_H

struct pixel {
  uint8_t b;
  uint8_t g;
  uint8_t r;
};

struct image {
  uint32_t width;
  uint32_t height;
  struct pixel *pixels;
};

struct image *create_image(const size_t width, const size_t height,
                           struct pixel *pixels);
struct image *create_an_empty_image(const uint32_t width,
                                    const uint32_t height);
void destroy_image(struct image *image);

#endif
