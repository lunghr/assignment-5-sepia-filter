#include "file_open_close.h"
#include <stdio.h>

enum open_status file_open_status(const char *fileName, FILE **file,
                                  const char *mode) {
  *file = fopen(fileName, mode);
  if (*file == NULL) {
    return OPEN_ERROR;
  }
  return OPEN_SUCCESS;
}

enum file_close_status close_status(FILE *file) {
  if (fclose(file) == EOF) {
    return CLOSE_ERROR;
  }
  return CLOSE_SUCCESS;
}
