#include "args_validator.h"

#include <stdbool.h>
#include <stdlib.h>

bool args_count_check(const int argc) {
  if (argc != 3 && argc != 4) {
    return false;
  }
  return true;
}

bool is_all_digits(const char *str) {
  if (*str == '-') {
    str++;
  }

  while (*str) {
    if (*str < '0' || *str > '9') {
      return false;
    }
    str++;
  }
  return true;
}

enum angle_status angel_check(const char *potentional_angle_value) {
  if (!is_all_digits(potentional_angle_value)) {
    return NOT_NUMERIC_ANGLE_VALUE;
  }

  switch (atoi(potentional_angle_value)) {
  case 0:
  case 90:
  case -90:
  case 180:
  case -180:
  case 270:
  case -270:
    return ANGLE_CORRECT;
  default:
    return INVALID_ANGLE_VALUE;
  }
}
