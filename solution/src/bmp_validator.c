#include "bmp_validator.h"

enum signature_file_status check_file_signature(struct bmp_header *header) {
  if (header->bf_type != BMP_SIGNATURE) {
    return UNKNOWN_FILE_SIGNATURE;
  }
  return BMP_FILE_SIGNATURE;
}

enum bmp_validation_status validate_bmp(struct bmp_header *header,
                                        enum signature_file_status signature) {
  if (signature != BMP_FILE_SIGNATURE) {
    return INVALID_FILE_FORMAT;
  }

  if (header->bi_size != BI_INFORMATION_HEADER_SIZE) {
    return INVALID_BMP_HEADER;
  }

  if (header->bi_width <= 0 || header->bi_height <= 0) {
    return INVALID_BMP_DATA;
  }

  if (header->bi_compression != BI_COMPRESSION) {
    return INVALID_BMP_DATA;
  }

  return VALIDATION_SUCCESS;
}
