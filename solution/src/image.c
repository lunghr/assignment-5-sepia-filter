#include "image.h"
#include <stdint.h>
#include <stdlib.h>

// image storage
struct image *create_image(const size_t width, const size_t height,
                           struct pixel *pixels) {
  struct image *img = malloc(sizeof(struct image));

  if (!img) {
    return NULL;
  }

  img->pixels = pixels;
  img->height = height;
  img->width = width;

  return img;
}

struct image *create_an_empty_image(const uint32_t width,
                                    const uint32_t height) {
  struct pixel *pixels = malloc(sizeof(struct pixel) * height * width);

  if (!pixels) {
    return NULL;
  }
  struct image *img = create_image(width, height, pixels);

  if (!img) {
    free(pixels);
  }

  return img;
}

void destroy_image(struct image *image) {
  free(image->pixels);
  free(image);
}
