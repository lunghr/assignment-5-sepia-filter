#define _POSIX_C_SOURCE 199309L

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "args_validator.h"
#include "bmp_header.h"
#include "file_open_close.h"
#include "filter.h"
#include "image_bmp_converter.h"


int main(int argc, char **argv) {

  bool argc_status = args_count_check(argc);
  if (!argc_status) {
    fprintf(stderr, "Incorrect count of arguments");
    exit(EXIT_FAILURE);
  }

  const char *f_input = argv[1];
  FILE *in;

  if (file_open_status(f_input, &in, "rb") != OPEN_SUCCESS) {
    fprintf(stderr, "An error occurred when opening a file %s", f_input);
    exit(EXIT_FAILURE);
  }

  struct bmp_header header = {0};
  if (!read_header(in, &header)) {
    fprintf(stderr, "Something wrong  with file header");
    exit(EXIT_FAILURE);
  }

  enum signature_file_status signature = check_file_signature(&header);
  enum bmp_validation_status validation_status =
      validate_bmp(&header, signature);
  if (validation_status != VALIDATION_SUCCESS) {
    fprintf(stderr, "I don't know what went wrong, but something went wrong");
    exit(EXIT_FAILURE);
  }

  struct image *not_rotated_image = convert_to_image(&header, in);
  if (not_rotated_image == NULL) {
    fprintf(stderr, "Failed to convert image from bmp format");
    exit(EXIT_FAILURE);
  }

  struct image *finally;
  struct timespec transformation_start = {0, 0}, transformation_end = {0, 0};
  clock_gettime(CLOCK_MONOTONIC, &transformation_start);
  if (argc == 3) {
    finally = filter(not_rotated_image);
  } else {
    finally = simd_filter(not_rotated_image);
  }
  clock_gettime(CLOCK_MONOTONIC, &transformation_end);

  const char *f_out = argv[2];
  FILE *out;

  if (file_open_status(f_out, &out, "wb") != OPEN_SUCCESS) {
    fprintf(stderr, "An error occurred when opening a file %s", f_out);
    exit(EXIT_FAILURE);
  }

  enum file_write_status idk = convert_to_bmp(out, finally);
  if (idk == WRITE_ERROR) {
    fprintf(stderr, "An error occurred when writing a file %s", f_out);
    exit(EXIT_FAILURE);
  }

  if (close_status(in) == CLOSE_ERROR) {
    fprintf(stderr, "An error occurred when closing a file %s", f_input);
    exit(EXIT_FAILURE);
  }

  if (close_status(out) == CLOSE_ERROR) {
    fprintf(stderr, "An error occurred when closing a file %s", f_out);
    exit(EXIT_FAILURE);
  }

  destroy_image(finally);
  printf("%zu", transformation_end.tv_nsec - transformation_start.tv_nsec);
  exit(EXIT_SUCCESS);
}
