#include "filter.h"
#include "image.h"

#include <stdint.h>
#include <stdlib.h>

struct image *filter(struct image *image) {
  struct image *result = image;

  for (size_t it = 0; it < image->width; ++it) {
    for (size_t jt = 0; jt < image->height; ++jt) {
      uint8_t r_input = image->pixels[it * image->height + jt].r;
      uint8_t g_input = image->pixels[it * image->height + jt].g;
      uint8_t b_input = image->pixels[it * image->height + jt].b;

      double r_processed =
          (r_input * .393) + (g_input * .769) + (b_input * .189);
      double g_processed =
          (r_input * .349) + (g_input * .686) + (b_input * .168);
      double b_processed =
          (r_input * .272) + (g_input * .534) + (b_input * .131);

      result->pixels[it * image->height + jt].r =
          r_processed > 0xFF ? 0xFF : (uint8_t)(r_processed);
      result->pixels[it * image->height + jt].g =
          g_processed > 0xFF ? 0xFF : (uint8_t)(g_processed);
      result->pixels[it * image->height + jt].b =
          b_processed > 0xFF ? 0xFF : (uint8_t)(b_processed);
    }
  }
  return result;
}
