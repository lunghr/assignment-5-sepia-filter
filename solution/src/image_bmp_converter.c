#include "image_bmp_converter.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

bool read_header(FILE *file, struct bmp_header *header) {
  return fread(header, sizeof(struct bmp_header), 1, file);
}

struct image *convert_to_image(struct bmp_header *header, FILE *file) {
  struct image *image =
      create_an_empty_image(header->bi_width, header->bi_height);
  if (image == NULL) {
    return NULL;
  }

  uint8_t offset_bytes = 4 - (header->bi_width * sizeof(struct pixel)) % 4;

  if (fseek(file, (long)header->bf_off_bits, SEEK_SET) != 0) {
    destroy_image(image);
    return NULL;
  }

  for (size_t i = 0; i < header->bi_height; ++i) {
    for (size_t j = 0; j < header->bi_width; ++j) {
      struct pixel tmp;
      if (fread(&tmp, sizeof(struct pixel), 1, file) != 1) {
        destroy_image(image);
        return NULL;
      }

      image->pixels[j + i * header->bi_width] = tmp;

      if (j == header->bi_width - 1 && offset_bytes != 0) {
        if (fseek(file, offset_bytes, SEEK_CUR) != 0) {
          destroy_image(image);
          return NULL;
        }
      }
    }
  }

  return image;
}

enum file_write_status convert_to_bmp(FILE *out, struct image *img) {
  if (!out || !img) {
    return WRITE_ERROR;
  }

  struct bmp_header header = {.bf_type = 0x4D42,
                              .bf_size = 0,
                              .bf_res = 0,
                              .bf_off_bits = sizeof(struct bmp_header),
                              .bi_size = sizeof(struct bmp_header) - 14,
                              .bi_width = (uint32_t)img->width,
                              .bi_height = (uint32_t)img->height,
                              .bi_planes = 1,
                              .bi_bit_count = 24,
                              .bi_compression = 0,
                              .bi_size_image = 0,
                              .bi_X_pels_mer_meter = 0,
                              .bi_Y_pels_per_meter = 0,
                              .bi_clr_used = 0,
                              .bi_clr_important = 0};

  header.bi_size_image = img->width * img->height * sizeof(struct pixel);
  header.bf_size = header.bf_off_bits + header.bi_size_image;

  if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
    return WRITE_ERROR;
  }

  uint8_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;

  for (uint64_t y = 0; y < img->height; ++y) {
    for (uint64_t x = 0; x < img->width; ++x) {
      if (fwrite(&img->pixels[y * img->width + x], sizeof(struct pixel), 1,
                 out) != 1) {
        return WRITE_ERROR;
      }
    }
    for (int p = 0; p < padding; p++) {
      fputc(0, out);
    }
  }

  return WRITE_SUCCESS;
}
